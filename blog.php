<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-header.php'); ?>
 		<!-- JQ UI -->
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<link rel="stylesheet" href="/css/jquery-ui.css" />
		<script src="/js/jquery-ui.js"></script>
		<script>
			$(function() {
				$( "#accordion" ).accordion({
					collapsible: true
				});
				$( "#accordion" ).accordion({
					active: false
				});
			});
		</script>
		<style>
			#accordion h3 {
				text-align: center;
				color: #c6342e;
				font-size: 16px;
				font-weight: bold;
			}
				#accordion div {
					width: 100%;
					min-height: 50px;
				}
				#accordion div #blog_form {
					font-size: 12px;	
				}
				#accordion div #blog_form input[type="text"] {
					padding: 5px;
					margin: 5px 0;
				}
				#accordion div #blog_form input[type="submit"] {
					padding: 5px;
					margin-top:5px;
				}
				#accordion div #blog_form textarea {
					resize: none;
					width: 300px;
					height: 80px;
					overflow-x: hidden;
				}
			#comment-area {
				margin-top:20px;
			}
			.fb-comments {
				/*height: 145px;
				width: 100%;
				overflow: auto;*/
			}
		</style>
	</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>	
	
	<div id="content">
		<div id="main">	
			
			<div id="body">
				<h2>Blog</h2>
				<span id="body_text">
	<?php
		if(loggedIn() && (getAnyUsersField_userid($conn, $_SESSION['user_id'], "admin") == 1)) { ?>
					<div id="accordion">
						<h3>Click here to add a blog entry.</h3>
						<div>
							<p>
								<form id="blog_form">
									<input type="text" placeholder="Title" id="blog_title" /><br>
									<textarea id="blog_msg" placeholder="Message" onKeyDown="textCount('blog_msg', 'countDisplay');" onKeyUp="textCount('blog_msg', 'countDisplay');"></textarea>
									<h6 id="countDisplay">255 characters remaining</h6><br>
									<!--<input type="text" placeholder="Youtube Video URL" id="ytVideo" />
									<span style="margin-left:20px;font-size:12px;color:red;">YouTube URL not required</span><br>-->
									<input type="submit" value="Blog" id="submit_blog" />
									<span id="feedback_b"></span>
								</form>								
							</p>
						</div>
					</div>
	<?php } ?>
					<?php getBlogPosts($conn); ?>
				</span>
			</div>			
		</div>
		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>
	
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/noJQInclude-main-footer.php'); ?>