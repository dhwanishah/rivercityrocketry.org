<?php


function checklogin2() {
	if (!loggedin()) {
		header('Location: ' . $structs["urls"]["main_url"]);
	}
}

function loggedIn() {
    if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
		return true;
    } else {
		return false;
    }
}

/*function isAdmin() {
	if (loggedIn()) {
		if(getAnyUsersField_userid($conn, $_SESSION['user_id'], "admin") === 1) {
			return true;
		} else {
			return false;
		}
	}
}*/

function randomKey21() {
	for($i = 0; $i < 21; $i++){
		$rand =  $rand .  rand(); 
	}
	$rand = md5($rand);
	return substr($rand, 11);
}

function passwordCrypt($password, $key) {
	$hash = crypt($password, '$2a$07$' . $key . '$');
	return $hash;	
}

function sendEmail($email, $validation_rk) {
	$to  = $email;
	$subject = 'Rivercityrocketry: Validation Email';
	$message = 'Please validate your email before you continue! Click on this link: http://www.rivercityrocketry.org/core/auth/validate.php?vk=' . $validation_rk . '&e=' . $email . ' or simply copy and paste it into the browser.';
	$headers = 'From: Rivercityrocketry.org <donotreply@rivercityrocketry.org>' . "\r\n";
	// Mail it
	mail($to, $subject, $message, $headers);	
}

function checkValidationKey($conn, $key, $email) {
   $key = htmlentities($key);
   $query = $conn->prepare('SELECT `id` FROM `users` WHERE `user_randkey`=? AND `email`=?');
   $query->bindParam(1, $key);
   $query->bindParam(2, $email);
   $query->execute();
   $result = $query->fetchAll();
   if (count($result) == 1) {
   	return true;
   } else {
   	return false;
   }
   $query = null;
}


?>