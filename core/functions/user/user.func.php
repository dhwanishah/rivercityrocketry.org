<?php

function getUserField($conn, $field) {
    $field = htmlentities($field);
    $query = $conn->prepare('SELECT ' . $field . ' FROM `users` WHERE `id`=?');
    $query->bindParam(1, $_SESSION['user_id']);
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) == 1) {
    	return $result[0][$field];
    } 
    $query = null;
}

/** TODO 
function getAnyTableField($conn, $table, $field) {
	$table = htmlentities($table);
    $field = htmlentities($field);
    $query = $conn->prepare('SELECT ' . $field . ' FROM ' . $table . ' WHERE `id`=?');
    $query->bindParam(1, $_SESSION['user_id']);
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) == 1) {
    	return $result[0][$field];
    } 
    $query = null;
} **/

function getAnyUsersField_userid($conn, $user_id, $field) {
    $user_id = htmlentities($user_id);
    $field = htmlentities($field);
    $query = $conn->prepare('SELECT ' . $field . ' FROM `users` WHERE `id`=?');
    $query->bindParam(1, $user_id);
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) == 1) {
    	return $result[0][$field];
    } 
    $query = null;
}

function getAnyUsersField_email($conn, $email, $field) {
    $email= htmlentities($email);
    $field = htmlentities($field);
    $query = $conn->prepare('SELECT ' . $field . ' FROM `users` WHERE `email`=?');
    $query->bindParam(1, $email);
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) == 1) {
    	return $result[0][$field];
    } else {
    	return false; //if nothing returned
    }
    $query = null;
}

/* Gets any users selected field through providing its user_randkey */
function getAnyUsersField_userRandKey($conn, $user_randkey, $field) {
    $user_randkey = htmlentities($user_randkey);
    $field = htmlentities($field);
    $query = $conn->prepare('SELECT ' . $field . ' FROM `users` WHERE `user_randkey`=?');
    $query->bindParam(1, $user_randkey);
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) == 1) {
    	return $result[0][$field];
    } else {
    	return false; //if nothing returned
    }
    $query = null;
}

function updateAnyUsersConfirmStatus_userRandKeyAndEmail($conn, $user_randkey, $email) {
    $user_randkey = htmlentities($user_randkey);
    $query = $conn->prepare("UPDATE `users` SET `registered`='set' WHERE `user_randkey`=? AND `email`=?");
    $query->bindParam(1, $user_randkey);
    $query->bindParam(2, $email);
    if ($query->execute()) {
    	return true;
    } else {
    	return false; //if error occured
    }
    $query = null;
}

function getActiveSessions($conn) {
    $field = "sessionCount";
    $query = $conn->prepare('SELECT ' . $field . ' FROM `activeSessions` WHERE `id`=1');
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) == 1) {
    	return $result[0][$field];
    } 
    $query = null;
}

function updateActiveUserSession($conn, $operation) {
	$count = getActiveSessions($conn);
	if ($operation === "add") {
		$count = $count + 1;
	}
	if (($count >= 0) && ($operation === "sub")) {
		$count = $count - 1;
	}
	$query = $conn->prepare("UPDATE `activeSessions` SET `sessionCount`='" . $count . "' WHERE `id`=1");
	if ($query->execute()) {
    	return true;
    } else {
    	return false; //if error occured
    }
    $query = null;
}

?>