<?php

function getBlogPosts($conn) {
    $query = $conn->prepare('SELECT `creator`, `title`, `post`, `timestamp` FROM `Blog` ORDER BY `id` DESC');
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) >= 1) {
    	for ($i = 0; $i < count($result); $i++) {
    		$title = $result[$i][1];
			$name = "name";
			$publish_date = $result[$i][3];
			$body = $result[$i][2];
			echo '<div id="blog_post">
				  	<div id="blog_header">
				  		<div id="blog_title">' . $result[$i][1] . '</div>
				  		<div id="blog_publish_date">Submitted by <b>'. getAnyUsersField_userid($conn, $result[$i][0], "email") . '</b> on <i>' . $result[$i][3] . '</i></div>
				  	</div>
				  	<hr>
				  	<div id="blog_body">' . $result[$i][2] . '</div>
				  	
				  	
				  	<div id="comment-area">
					  	<h3>Comment on this post.</h3>
					  	<div class="fb-comments" href="http://www.rivercityrocketry.org/blog.php?post=' . $i . '" numposts="2" colorscheme="light"></div>			  
				  	</div>	
				  	
				  	
				  </div>';
    	}
    } else {
    	echo '<div id="blog_post">No blog entries were found.</div>';
    } 
    $query = null;
}

function getBlogPostTitleForSidebar($conn) {
    $query = $conn->prepare('SELECT `title` FROM `Blog` ORDER BY `id` DESC LIMIT 3');
    $query->execute();
    $result = $query->fetchAll();
    if (count($result) >= 1) {
    	for ($i = 0; $i < count($result); $i++) {
    		$title = $result[$i][0];
		echo '<a href="blog.php"><li>' . $title . '</li></a>';
    	}
    } else {
    	echo '<li>No updates</li>';
    } 
    $query = null;
}

?>