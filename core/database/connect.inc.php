<?php
/* structures file requires is already going to be imported in core.inc.php where this files is also required so no need to
   require it for the  
*/

$host = $structs["database"]["db1"]["host"];
$database = $structs["database"]["db1"]["db_name"];
$user = $structs["database"]["db1"]["user"];
$password = $structs["database"]["db1"]["password"]; 

$conn_err = "Cannot connect to the database at this time. Please try again later!";

/*if(!@mysql_connect($mysql_host, $mysql_user, $mysql_password) || !@mysql_select_db($mysql_database)) {
    die($conn_err);
}*/

try {
	$conn = new PDO('mysql:host=' . $host . ';dbname=' . $database, $user, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
} catch(PDOException $e) {
	echo 'ERROR: ' . $e->getMessage();
}


?>