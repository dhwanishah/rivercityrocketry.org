<?php
/* Need to require this because these pages are ajax loads thus are not 
getting the require from the header.php file */

require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');

?>

<?php

if(isset($_POST['blog_title']) && isset($_POST['blog_msg'])) {// && isset($_POST['ytVideo'])) {
    $blog_title = htmlentities($_POST['blog_title']);
    $blog_msg = htmlentities($_POST['blog_msg']);
    /*if (!empty($_POST['ytVideo'])) {
	$ytVideo = '<iframe width="560" height="315" src="' . $_POST['ytVideo'] . '" frameborder="0" allowfullscreen></iframe>';
	$blog_msg = $blog_msg . "<br><br>" . $ytVideo;
    }*/
    $msg = '';
    if(!empty($blog_title) && !empty($blog_msg)) {	    		
	$query = $conn->prepare("INSERT INTO `Blog` VALUES ('', ?, ?, ?, NOW())");
	$query->bindParam(1, $_SESSION['user_id']);
	$query->bindParam(2, $blog_title);
	$query->bindParam(3, $blog_msg);
	$query->execute();
	$msg = 'Your blog has been posted.<script>location.reload();</script>';
	echo $msg;		    	     	
    } else {
	$msg = 'All field are required!';
	echo $msg;
    }
}
?>

	