<?php
ob_start(); /*For header function usage */
session_start(); /*starting session */
$http_referer = $_SERVER['HTTP_REFERER'];

if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
	$user_id = $_SESSION['user_id'];
}


require_once($_SERVER['DOCUMENT_ROOT'] . '/core/struct.req.php'); /*import structure of the site*/

	/*database connection*/
	require_once('database/connect.inc.php'); 
	
	/* global variables 
	require_once('global_vars/global_vars.php');*/
	
	/*function file include*/
		/* user */
			require_once('functions/user/user.func.php'); //user functions
			require_once('functions/user/auth.func.php'); //authentication functions
		/* items */	
			require_once('functions/blog/blog.func.php');


?>