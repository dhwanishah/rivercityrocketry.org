<?php
/* Need to require this because these pages are ajax loads thus are not 
getting the require from the header.php file */

require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');
?>


<?php
	$validation_key = $_GET['vk'];
	$key_email = $_GET['e'];
	if (isset($validation_key) && !empty($validation_key) && isset($key_email) && !empty($key_email)) {
		if (checkValidationKey($conn, $validation_key, $key_email)) {
			if (getAnyUsersField_userRandKey($conn, $validation_key, 'registered') === 'null') {
				if (updateAnyUsersConfirmStatus_userRandKeyAndEmail($conn, $validation_key, $key_email)) {
					echo 'You have been confirmed, you may now login!';
				} else {
					echo 'There was an error! Our robots have been disbatched to fix it!';
				}
			} else {
				echo 'You have already been confirmed! Just login...';
			}			
		} else {
			echo 'That\'s not a correct validation key!';
		}
	} else {
		header("Location: /");
	}
?>
