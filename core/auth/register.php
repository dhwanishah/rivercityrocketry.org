<?php
/* Need to require this because these pages are ajax loads thus are not 
getting the require from the header.php file */

require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');

?>

<?php

if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['confirm_password']) && isset($_POST['sec_check'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password_again = $_POST['confirm_password'];
    $honeypot_secCheck = $_POST['sec_check'];
    $msg = '';
    if($honeypot_secCheck == "") {
	    if(!empty($email) && !empty($password) && !empty($password_again)) {
	    	if($password === $password_again) {
	    		if(getAnyUsersField_email($conn, $email, 'id') === false) {
	    			if(filter_var($email, FILTER_VALIDATE_EMAIL) != false) {
			    		$rk = randomKey21();
			    		$password_hash = passwordCrypt($password, $rk);    		
			    		$query = $conn->prepare("INSERT INTO `users` VALUES ('', ?, ?, ?, NOW(), 'null', '0')");
					$query->bindParam(1, $email);
					$query->bindParam(2, $password_hash);
					$query->bindParam(3, $rk);
					$query->execute();
					sendEmail($email, $rk);		
			    		echo 'You have been registered, please check your email for the confirmation link.';		    	

				} else {
				   	$msg = 'Thats not a valid email address!';
		    			echo $msg;
				}
		    	} else {
		    		$msg = 'That email already exists!';
		    		echo $msg;
		    	}	
	    	} else {
	    		$msg = 'The two passwords do not match!';
			echo $msg;
	    	}     	
	    } else {
		$msg = 'All field are required!';
		echo $msg;
	    }
    } else {
    	$msg = 'Security Breach!';
	echo $msg;
    }
}
?>

	