<?php
/* Need to require this because these pages are ajax loads thus are not 
getting the require from the header.php file */

require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');

?>

<?php
if(isset($_POST['email']) && isset($_POST['password'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $entered_user_rk = getAnyUsersField_email($conn, $email, 'user_randkey');
    $password_hash = crypt($password, '$2a$07$' . $entered_user_rk . '$');
    $msg = '';
    if(!empty($email) && !empty($password)) {
    	if(filter_var($email, FILTER_VALIDATE_EMAIL) != false) {	    	
	    	$query = $conn->prepare("SELECT `id` FROM `users` WHERE `email`=? AND `password`=?");
		$query->bindParam(1, $email);
		$query->bindParam(2, $password_hash);
		$query->execute();
		$result = $query->fetchAll();
		if(count($result) == 1) {
			if (getAnyUsersField_email($conn, $email, 'registered') != 'null') {
				$msg = 'Logging in...';
				echo $msg;
				$user_id = $result[0]['id'];
				$_SESSION['user_id'] = $user_id;
				updateActiveUserSession($conn, "add");
				echo '<script type="text/javascript">top.location.reload();</script>'; //use this instead of header since ajax is used for auth here
			} else {
				$msg = 'You have not confirmed your email address. Check your email!';
				echo $msg;
			}
		} else {
			$msg = 'Invalid email/password combination!';
			echo $msg;
		}
	} else {
		$msg = 'Thats not a valid email address!';
		echo $msg;
	}
    } else {
	$msg = 'Please supply a email and password!';
	echo $msg;
    }    
}
?>