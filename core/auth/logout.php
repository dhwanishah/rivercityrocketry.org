<?php
/* Need to require this because these pages are ajax loads thus are not 
getting the require from the header.php file */

require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');


session_destroy();
updateActiveUserSession($conn, "sub");
header('Location: ' . $http_referer);

?>