<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-header.php'); ?>
		
		<!-- JQ UI -->
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<link rel="stylesheet" href="/css/jquery-ui.css" />
		<script src="/js/jquery-ui.js"></script>
		<script>
		 $(function() {
			$( "#tabs" ).tabs();
		 });
		</script>
		
	</head>
<body>
<style>

</style>
<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>	
		
	<div id="content">
		<div id="main">	
			
			<div id="body">
				<h2>Documents</h2>
				<span id="body_text">
				
				<div id="tabs">
					 <ul>
						<li><a href="#tabs-1">2013-2014</a></li>
						<li><a href="#tabs-2">2012-2013</a></li>
						<li><a href="#tabs-3">2011-2012</a></li>
					</ul>
					<div id="tabs-1">
						<p>
						<div id="documents_list">						
							<div class="document_cat">
							<h3>Proposal</h3>
								<a href="/documents/2013-2014/uofl2013-2014_proposal.pdf" target="_blank">Proposal</a>	
							</div>
							<div class="document_cat">
							<h3>Preliminary Design Review (PDR)</h3>
								<a href="/documents/2013-2014/pdr_2013-2014.pdf" target="_blank">PDR</a>
								<a href="/documents/2013-2014/pdr_flysheet_2013-2014.pdf" target="_blank">PDR Flysheet</a>
								<a href="/documents/2013-2014/pdr_presentation_2013-2014.pdf" target="_blank">PDR Presentation</a>
							</div>
							<div class="document_cat">
							<h3>Critical Design Review (CDR)</h3>
								<a href="/documents/2013-2014/cdr_2013-2014.pdf" target="_blank">CDR</a>
								<a href="/documents/2013-2014/cdr_flysheet_2013-2014.pdf" target="_blank">CDR Flysheet</a>
								<a href="/documents/2013-2014/cdr_presentation_2013-2014.pdf" target="_blank">CDR Presentation</a>
								<a href="/documents/2013-2014/cdr_safety_lab_manual.pdf" target="_blank">Safety Lab Manual</a>
								<a href="/documents/2013-2014/cdr_addendum_2013-2014.pdf" target="_blank">Addendum</a>			
							</div>
						</div>
						</p>
					</div>
					<div id="tabs-2">
						<p>
						<div id="documents_list">						
							<div class="document_cat">
							<h3>Proposal</h3>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_proposal.pdf" target="_blank">Proposal</a>
							</div>
							
							<div class="document_cat">
							<h3>Preliminary Design Review (PDR)</h3>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_pdr_0.pdf" target="_blank">PDR</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_pdr_compressed.pdf" target="_blank">PDR Compressed</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_pdr_flysheet.pdf" target="_blank">PDR Flysheet</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_pdr_presentation.pdf" target="_blank">PDR Presentation</a>
							</div>
							
							<div class="document_cat">
							<h3>Critical Design Review (CDR)</h3>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_cdr.pdf" target="_blank">CDR</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_cdr_compressed.pdf" target="_blank">CDR Compressed</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_cdr_flysheet.pdf" target="_blank">CDR Flysheet</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_cdr_presentation.pdf" target="_blank">CDR Presentation</a>
							</div>							
							<div class="document_cat">
							<h3>Flight Readiness Review (FRR)</h3>
								<a href="http://uoflusli.com/sites/default/files/documents/2013uofl_frr.pdf" target="_blank">FRR</a>
								<a href="http://uoflusli.com/sites/default/files/documents/2013uofl_frr_compressed.pdf" target="_blank">FRR Compressed</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_frr_flysheet.pdf" target="_blank">FRR Flysheet</a>
								<a href="http://uoflusli.com/sites/default/files/documents/uofl2013_frr_presentation.pdf" target="_blank">FRR Presentation</a>
							</div>
							<div class="document_cat">
							<h3>Misc. Documents</h3>
								<a href="http://uoflusli.com/sites/default/files/documents/2012-2013%20Sponsorship%20Packet.pdf" target="_blank">Sponsorship Packet</a>
								<a href="http://uoflusli.com/sites/default/files/documents/UofL%20Cardinal%20Article.pdf" target="_blank">UofL Cardinal Article</a>
							</div>
						</div>
						</p>
					</div>
					<div id="tabs-3">
						<p>
						<div id="documents_list">						
							<div class="document_cat">
							<h3>Proposal</h3>
								<a href="/documents/2011-2012/UofL_Proposal_2011-2012.pdf" target="_blank">Proposal</a>	
							</div>
							<div class="document_cat">
							<h3>Preliminary Design Review (PDR)</h3>
								<a href="/documents/2011-2012/UofL_pdr_2011-2012.pdf" target="_blank">PDR</a>	
								<a href="/documents/2011-2012/pdr_flysheet_2011-2012.pdf" target="_blank">PDR Flysheet</a>
								<a href="/documents/2011-2012/pdr_presentation_2011-2012.pdf" target="_blank">PDR Presentation</a>	
							</div>
							<div class="document_cat">
							<h3>Critical Design Review (CDR)</h3>
								<a href="/documents/2011-2012/UofL_cdr_2011-2012.pdf" target="_blank">CDR</a>	
								<a href="/documents/2011-2012/cdr_flysheet_2011-2012.pdf" target="_blank">CDR Flysheet</a>
								<a href="/documents/2011-2012/cdr_presentation_2011-2012.pdf" target="_blank">CDR Presentation</a>
							</div>
							<div class="document_cat">
							<h3>Flight Readiness Review (FRR)</h3>
								<a href="/documents/2011-2012/uofl_frr_2011-2012.pdf" target="_blank">FRR</a>
								<a href="/documents/2011-2012/frr_presentation_2011-2012.pdf" target="_blank">FRR Flysheet</a>
								<a href="/documents/2011-2012/frr_presentation_2011-2012.pdf" target="_blank">FRR Presentation</a>	
							</div>
							<div class="document_cat">
							<h3>Post Launch Assessment Review (PLAR)</h3>
								<a href="/documents/2011-2012/UofL_plar_2011-2012.pdf" target="_blank">PLAR</a>	
							</div>
						</div>
						</p>
					</div>
				</div> <!-- end tabs -->
				</span>
			</div>			
		</div>
		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>
	
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/noJQInclude-main-footer.php'); ?>