<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-header.php'); ?>

	</head>
<body>
<style>

</style>
<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>
	
	<div id="content">
		<div id="main">
			<div id="sliderFrame" style="margin-bottom:60px;"> 
				<div id="slider">
					<a href="http://rivercityrocketry.org/">
						<img src="/img/imgSlider_photos/2013-2014-team2.jpg" alt="University of Louisville 2013-2014 NSL Team" />
					</a>
					<a href="http://rivercityrocketry.org/">
						<img src="/img/imgSlider_photos/outreach.jpg" alt="Educational student outreach" />
					</a>
					<a href="http://rivercityrocketry.org/">
						<img src="/img/imgSlider_photos/subscale-launch-justin.jpg" alt="Subscale Launches" />
					</a>
					<a href="http://rivercityrocketry.org/">
						<img src="/img/imgSlider_photos/2012-2013-with-rocket.jpg" alt="2012-2013 USLI Competition - 2nd Place Overall Winners!"/>
					</a>
					<a href="http://rivercityrocketry.org/">
						<img src="/img/imgSlider_photos/kyle_outreach.jpg" />
					</a>
					<!--<a href="http://rivercityrocketry.org/">
						<img src="/img/imgSlider_photos/team2012.jpg" alt="#htmlcaption" />
					</a>-->
				</div>
				<!--<div id="htmlcaption" style="display: none;">
					<em>HTML</em> caption. Link to <a href="http://www.google.com/">Google</a>.
				</div>-->
			</div>
			
			<div id="body">
				<h2 style="text-align:center">Welcome to the University of Louisville - River City Rocketry Homepage</h2>
				<span id="body_text">
				<p>
				<span class="tab">Formed</span> in the summer of 2011, Rivercity Rockety, formly known as UofL USL, is a group that competes in the annual NASA Student Launch Projects NSL competition and other similar rocketry competitions. Coming from the University of Louisville J.B. Speed School of Engineering, our team consists of members from multiple fields of engineering, all whom have come together from an interest for aerospace and aeronautics. Our goal is to create the most intricate, out-of-the-box rocket NASA, or anyone else, has ever seen in the competition. Check out the rest of our page for details about our project, sponsors, membership, and the many Educational Outreach events our group takes part in.
				</p>
				<p>
				<span class="tab">Thank</span> you for visiting our page, and if you have any questions, comments, or would like to speak with us about funding, educational outreach, or technical advice please send us an email at <a href="mailto:rivercityrocketry@gmail.com" target="_top"><b>rivercityrocketry@gmail.com</b></a>.
				</p>
				</span>
			</div>
		</div>
		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-footer.php'); ?>