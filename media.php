<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-header.php'); ?>

	</head>
<body>
<style>

</style>
<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>	
	
	<div id="content">
		<div id="main">	
			
			<div id="body">
				<h2>Media</h2>
				<span id="body_text" class="media_body">
					<p><b>
					Over the years, our Rocketry team has gained so much attention from the media.<br>
					Here are some instances of our publicity around the nation:
					</b></p><br>

					<a href="https://lh6.googleusercontent.com/-EeIhFXu1niE/URC0vzzmeMI/AAAAAAAAByw/rmEHoDuyS-g/s826/LouisvilleCardinalAug21-2012-Cover.jpg" data-lightbox="roadtrip"><img src="https://lh6.googleusercontent.com/-EeIhFXu1niE/URC0vzzmeMI/AAAAAAAAByw/rmEHoDuyS-g/s826/LouisvilleCardinalAug21-2012-Cover.jpg" width=220 height=320 /></a>
					<a href="https://lh4.googleusercontent.com/-kDBPzzp2zgY/URC0wYoxsdI/AAAAAAAABy8/9yaLfDmoXEg/s791/LouisvilleCardinalAug21-2012-Page1.jpg" data-lightbox="roadtrip"><img src="https://lh4.googleusercontent.com/-kDBPzzp2zgY/URC0wYoxsdI/AAAAAAAABy8/9yaLfDmoXEg/s791/LouisvilleCardinalAug21-2012-Page1.jpg" width=220 height=320 /></a>
					<a href="https://lh3.googleusercontent.com/-DBgEVsJG24o/URC0wEdB0FI/AAAAAAAABy0/q38C5G59Cuk/s791/LouisvilleCardinalAug21-2012-Page2.jpg" data-lightbox="roadtrip"><img src="https://lh3.googleusercontent.com/-DBgEVsJG24o/URC0wEdB0FI/AAAAAAAABy0/q38C5G59Cuk/s791/LouisvilleCardinalAug21-2012-Page2.jpg" width=220 height=320 /></a>
					
					
					<p><a href="http://www.louisvillecardinal.com/2012/01/small-team-engineers-compete-nasa-rocket-design/">http://www.louisvillecardinal.com/2012/01/small-team-engineers-compete-n...</a></p>
<p><a href="http://www.wdrb.com/story/20657518/the-rocketeers-u-of-l">http://www.wdrb.com/story/20657518/the-rocketeers-u-of-l</a></p>
<p><a href="http://www.worldwidelearn.com/education-articles/10-epic-projects-led-by-college-students.html">http://www.worldwidelearn.com/education-articles/10-epic-projects-led-by...</a></p>
<p><a href="http://www.nasa.gov/offices/education/programs/descriptions/Student_Launch_Projects.html">http://www.nasa.gov/offices/education/programs/descriptions/Student_Laun...</a></p>
					
				</span>
			</div>			
		</div>
		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-footer.php'); ?>