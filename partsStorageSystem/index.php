<?php include('core/core.inc.php'); ?>
<!doctype html>
<html>
<head>
	<title>Parts Storage System</title>
	<link rel="stylesheet" href="ProductSysStyle.css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	 <script>
		$(function() {
			$( "#datepicker" ).datepicker();
		});
	</script>
</head>
<body>

<form> 
     Part Number:<br><input type="text" id="partNumber" name="partNumber" /><br>
     Description:<br><input type="text" id="description" name="description" /><br>
     Date:<br><input type="text" name="date" id="datepicker" /><br>
     Initials:<br><input type="text" id="initials" name="initials" /><br>
     <!--<input type="submit" value="Add Part Number" name="new_part_submit" />-->
     <input type="submit" value="Save" id="save_submit" name="save_submit" />
     <!--<input type="submit" value="Exit" name="exit_submit" />-->
     <span id="feedback"></span>
     <input type="submit" value="Refresh" id="refresh_submit" name="refresh_submit" />
</form>

<div id="data_output">
	<?php getProductList($conn); ?>
</div>

<script type="text/javascript" src="js/submit.js"></script>
<script>
	/*$("#refresh_submit").click(function() {
		$('#data_output').html("<?php getProductList($conn); ?>")
	});*/
</script>

</body>
</html>