<?php 

//include($_SERVER['DOCUMENT_ROOT'] . '/partsStorageSystem/core/funcs/func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/partsStorageSystem/core/core.inc.php');

if (isset($_POST['partNumber']) && isset($_POST['description']) && isset($_POST['date']) && isset($_POST['initials'])) {
	$partNum = $_POST['partNumber'];
	$desc = $_POST['description'];
	$date = $_POST['date'];
	$initials = $_POST['initials'];
	if (empty($partNum) && empty($desc) && empty($date) && empty($initials)) {
		echo "<br>One or more fields are empty...and they can't be!";
	} else {
		if ((strlen($desc) <= 255)) {  		
			$query = $conn->prepare("INSERT INTO `PartsDatabase` VALUES ('', ?, ?, ?, ?)");
			$query->bindParam(1, $partNum);
			$query->bindParam(2, $desc);
			$query->bindParam(3, $date);
			$query->bindParam(4, $initials);
			if($query->execute()) {
				echo '<br><span style="color:#62AC44;line-height:20px;">Entry has been added!</span>';
			} else {
				echo '<br>There was problem...but don\'t worry, our robots have been disbatched!';
			}    		
		}
	}
}
	
?>