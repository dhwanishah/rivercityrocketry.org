$(document).ready(function() {
	saveSubmit();	
	//refreshSubmit();
});

function saveSubmit() {
	$('#partNumber').focus(); //focus on username field on page load
	$('#feedback').load('/partsStorageSystem/core/funcs/submit_feedback.inc.php').fadeIn(1000);
	$('#save_submit').click(function () {	
		$.ajax({
			type: 'POST',
			url: '/partsStorageSystem/core/funcs/submit_feedback.inc.php',
			data: ({ partNumber: $('#partNumber').val(), description: $('#description').val(), date: $('#datepicker').val(), initials: $('#initials').val() }),
			beforeSend: function() {
		            $('#feedback').html('<center><img src="../img/load.gif" alt="Loading..." /></center>');
		        },
			success: function(result) {
				$('#feedback').html(result).fadeIn(2000);
				//$('#data_output').html("<?php getProductList($conn); ?>").fadeIn(200);
				//alert('y');
			}
		});		
		return false;		
	});
}

/*function refreshSubmit() {
	$('#refresh_submit').click(function () {
		$('#data').load('http://rivercityrocketry.org/partsStorageSystem/core/funcs/getPartsList.func.inc.php');		
		return false;
	});
}*/