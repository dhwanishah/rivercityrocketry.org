<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-header.php'); ?>

	</head>
<body>
<style>

</style>
<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>	
	
	<div id="content">
		<div id="main">	
			<div id="body">
			<?php 
				if ($_GET['a'] == "teamCalendar") {
			?>			
				<h2>Team Calendar</h2>
				<span id="body_text">
					<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=rivercityrocketry%40gmail.com&amp;color=%23B1440E&amp;ctz=America%2FNew_York" style=" border-width:0 " width="800" height="600" frameborder="0" scrolling="no"></iframe>
				</span>			
			<?php
				} else if ($_GET['a'] == "supporters") {
			?>
				<h2>Our Supporters</h2>
				<span id="body_text">
				<p>
				<span class="tab">Our</span> group has been fortunate enough to form relations with several groups, individuals, and companies. Through monetary, manufacturing, and faculty support and donations, our group has an extremely bright outlook for the 2012-2013 season. We would like to give a big thanks to all of our supporters; we could not do nearly the job we are doing now without your help. Please take the time to click on the logos below and visit our supporters websites.
				</p>
				<a href="http://www.louisville.edu/" target="_blank"><img src="/img/supporters_logo/uofl.png" /></a>
				<a href="http://louisville.edu/speed/" target="_blank"><img src="/img/supporters_logo/speed%20school.png" /></a>
				<a href="http://louisville.edu/sga/" target="_blank"><img src="/img/supporters_logo/sga.png" /></a>
				<a href="http://www.nasa.gov/offices/education/programs/descriptions/Student_Launch_Projects.html" target="_blank"><img src="/img/supporters_logo/nasa.png" /></a>
				<a href="http://www.statefarm.com/agent/US/KY/Owensboro/George-Greco-5ZZN81YS000" target="_blank"><img src="/img/supporters_logo/statefarm.png" /></a>
				<a href="http://nasa.engr.uky.edu/" target="_blank"><img src="/img/supporters_logo/kyspacegrantcon.png" /></a>
				<a href="http://www.samtec.com/" target="_blank"><img src="/img/supporters_logo/samtec.png" /></a>
				<a href="http://www.onealsteel.com/" target="_blank"><img src="/img/supporters_logo/oneal.png" /></a>
				<a href="http://www.bluesrocks.org/" target="_blank"><img src="/img/supporters_logo/bluegrassrocketry.png" /></a>
				<a href="http://www.jmttool.com/" target="_blank"><img src="/img/supporters_logo/jmt.png" /></a>
				<a href="http://www.wildmanrocketry.com/" target="_blank"><img src="/img/supporters_logo/windman.png" /></a>
				<a href="http://www.kysciencecenter.org/" target="_blank"><img src="/img/supporters_logo/sciencecenter.png" /></a>
				<a href=""><img src="/img/supporters_logo/nadc.png" /></a>	
				<a href="http://www.arcaderx.com/" target="_blank"><img src="/img/supporters_logo/louarcadeexpo.png" width="200px" /></a>
				<a href="http://www.lvl1.org/" target="_blank"><img src="/img/supporters_logo/lvl1.png" /></a>
				</span>
				<h2 style="margin-top:60px;">2014 RiverCityRocketry Logo Design</h2>
				<span id="body_text">
					<img src="http://payload264.cargocollective.com/1/15/499345/7585946/RCREMBLEM6large_905.png" style="width:150px;height:150px;" /><br>
					<strong style="font-size:20px;">
						Artist behind the magic: <a href="http://www.cargocollective.com/msnmcf" style="color:blue;">Mason McFarland</a>!
					</strong>
				</span>
			<?php	
				} else {				
			?>
				<h2>Projects</h2>
				<span id="body_text">
					<p><span class="tab">NASA</span> University Student Launch is an annual collegiate rocketry competition in which teams from all over the nation compete against one another. Teams are required to design, test, and build a high powered rocket to fly to one mile altitude exactly while carrying a scientific payload. Teams are also scored on technical writing, presentation, education outreach, and website professionalism.</p>
					<p><span class="tab">In</span> 2012, the University of Louisville formed its first USL team, consisting of 13 engineering students from Computer, Electrical, Mechanical, and Industrial Engineering majors. In our first year, the team was awarded Best Website and a 5th place overall finish. For being the highest ranking, first-year team we also received the Best Rookie Team award. In 2013, the University of Louisville also competed in the USL competition and placed 2nd place overall and took an astonishing feat in the awards category by taking three out of the six awards, including the best website award again.</p>				
					<p><span class="tab">This</span> year, the team is focusing on taking the competition to a whole new level. With a focus on designing every component with efficiency in mind, our rocket will tout many systems that may never have been thought of. Expanding the team to 16 students has allowed the incorporation of in-house machining of the vast majority of all components used.</p>
					<p><span class="tab">As</span> for last year, the team was also selected as 1 of 6 teams to include NASAs Science Mission Directorate payload. In essence, this system includes the capabilities to measure temperature, humidity, pressure, irradiance, GPS coordinates, and ultraviolet radiation. As a twist, our team utilized a Samsung Galaxy phone to take all measurements, broadcast them to our ground station, and update our website with content on the fly. We will also be actively updating our Facebook and Twitter pages with live content as the rocket soars to 5,280 feet.</p>
					<p><span class="tab">Our</span> second payload was, what we call, the Parachute Control System. Much like the drawstrings on a hoodie, a microcontroller and servo configuration will actively monitor the open diameter of our parachute. At apogee (or the highest point of flight) the system will let the rocket near-freefall by keeping the open diameter closed. As the rocket approaches the landing destination, the parachute will gradually open and close until a desirable landing velocity is obtained. Our goal is to minimize horizontal drift as much as possible, while also decreasing necessary bay space by using only one parachute for a "dual deployment" effect.</p>
					<p><span class="tab">Besides</span> designing, testing, and launching our competition rocket, the team actively participates in STEM educational engagement opportunities with local schools and groups. STEM simply stands for Science, Technology, Engineering, and Mathematics. By introducing students to these principals, we hope to inspire the next generation of rocketeers, astronauts, and engineers. Our goal this year is to reach over 1000 students with hands-on programs.</p>
					<p><span class="tab">Obviously,</span> funding is our major issue every year. Running a project on this level is difficult because it requires significant funding. With every additional dollar we receive, we are able to push the envelope further and further. Other teams participating in the competition see the bar being raised, and in turn, their programs excel as well. While the event is a competition, we are really pushing each other to perpetually excel the fields of aeronautics and astronautics.</p>
					<p><span class="tab">Launching</span> a high-powered rocket is also no easy feat. We spend 100s of hours designing our rocket to make sure nothing goes awry during tests. The money we receive from our sponsors helps ensure that we can push our payload and vehicle designs through effectively.</p>
				</span>			
			<?php
				}
			?>
			</div>
		</div>

		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'); ?>	
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-footer.php'); ?>