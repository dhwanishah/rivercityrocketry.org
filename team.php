<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-header.php'); ?>

	</head>
<body>
<style>

</style>
<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>	
	
	<div id="content">
		<div id="main">	
			
			<div id="body">
				<h2>Our Team</h2>
				<span id="body_text">
				
					<style>
						#arrow {
							content:"";
							width:0;
							height:0;
							border-left: 1px solid black;
							border-right: 1px solid transparent;
							border-top: 1px solid transparent;
							border-bottom: 1px solid transparent;
						}
						#team-group #member {
							margin: 5px;
							padding: 8px;
							background: #fff;
							display: block;
							width: 100%;
							overflow: hidden;
							box-shadow: 0px 0px 10px #ddd;
						}
						#member_pic {
							float: left;
							width: 15%;
							text-align: center;
						}
						#member_pic:after {
							width: 0; 
							height: 0; 
							border-top: 10px solid transparent;
							border-bottom: 10px solid transparent;	
							border-right:10px solid blue;
						}
						#member_pic img {
							max-width: 100px;
							max-height: 80px;							
							position: relative;
							padding: 3px;
							box-shadow: 0px 0px 15px #ddd;
						}
						#member_desc {
							float: left;
							width: 82%;
							margin-top: 5px;
							font-size: 14px;
							line-height: 16px;
						}
						#member_pic span {
							font-weight: bold;
							font-size: 14px;
							position: relative;
							top: 2px;
						}			
					</style>
					<div id="team-group">
						<div id="member">
							<div id="member_pic">								
								<img src="/img/Team/austin.jpg" /><br>
								<span>Austin Lassell</span>
							</div>
							<div id="member_desc">								
								Coupling a passion for engineering and a love for photography, Austin is pursuing a Bachelor’s of Science in Industrial Engineering as well as working as the Senior Photogrpaher for the Louisville Cardinal. As a member of the University of Louisville River City Rocketry team, he will use both his experience in photography and knowledge of engineering to assist the team in capturing pictures wherever needed, including onboard the rocket.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/carlos.jpg" class="team" /><br>
								<span>Carlos Gonzalez</span>
							</div>
							<div id="member_desc">
								Carlos is a foreign student from Mexico. He is currently in his 4th year at the University of Louisville studying Mechanical Engineering along with Electrical Engineering. He is interested in a career with mechatronic systems. Besides working on the rocket, Carlos plans to build a quad copter later this year once he gains more experience working with an Arduino microcontroller.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/dhwani.jpg" class="team" /><br>
								<span>Dhwani Shah</span>
							</div>
							<div id="member_desc">
								Dhwani is a senior student at the J.B. Speed school of Engineering majoring in Computer Engineering and Computer Science. He currently is contributing as a member on the electrical engineering team, helping with the back end programming tasks and is also the current webmaster.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/emily.jpg" class="team" /><br>
								<span>Emily Robison</span>
							</div>
							<div id="member_desc">
								Emily is currently a third year mechanical engineering student at the University of Louisville's J.B. Speed School of Engineering. She will be returning for  her second year on the team as the Safety Officer. She has always expressed an interest in aeronautics and spent two semesters on co-op working on various programs with Raytheon Missile Systems. Through this experience, she gained valuable knowledge in design, testing, assembly processes, and safety. Emily hopes to follow her dream of pursuing a career with Raytheon or another company in the aerospace industry.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/gregg.jpg" class="team" /><br>
								<span>Gregg Blincoe</span>
							</div>
							<div id="member_desc">
								Gregg is currently a fourth year mechanical engineering of Engineering. He learned a lot through his involvement in the 2012-2013 competition year and is co-leading the team this year. He successfully obtained both Level 1 and Level 2 certifications through Tripoli using both commercial and scratch built kits respectively. Gregg plans to apply his engineering knowledge and skills in the aerospace industry upon graduation from UofL.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/justin.jpg" class="team" /><br>
								<span>Justin Lord</span>
							</div>
							<div id="member_desc">
								Justin is currently a senior mechanical engineering student at J.B. Speed School of Engineering. After joining the team at the tail end of last season has gained his Level 1 certification through Tripoli using a kit built rocket. Justin plans on continuing school with the ultimate goal of getting a PhD in mechanical engineering.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/kareem.jpg" class="team" /><br>
								<span>Kareem Moulana</span>
							</div>
							<div id="member_desc">
								Kareem is a senior Electrical and Computer Engineering student at J.B. Speed School of Engineering and the Vice Chair of the IEEE student branch at UofL. He has expressed an interest in the controls industry. In addition to USL team, Kareem is also involved with UofL IEEE Robotics Team along with several other personal projects. Kareem is currently working with the Electrical Team.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/ben.jpg" class="team" /><br>
								<span>Benjamin Mueller</span>
							</div>
							<div id="member_desc">
								Benjamin is an individual who loves to explore and learn about new things.  He joined the team from an interest in military hardware including missiles and jets such as the Sukhoi Su-47 Berkut.  He enjoys fiction books, story development, video games and an array of sports and activities including: golf, tennis, and martial arts.  Benjamin is a sophomore, and looking forward to his first co-op in the spring of 2013.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/daniel.jpg" class="team" /><br>
								<span>Daniel Neel</span>
							</div>
							<div id="member_desc">
								Daniel is a senior CECS student at U of L. Daniel enjoys programming, especially making games, and has had an interest in space exploration for a while. When not working on the rocket team, Daniel enjoys making music and learning about a wide variety of topics.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/patrick.jpg" class="team" /><br>
								<span>Patrick Duffy</span>
							</div>
							<div id="member_desc">
								Patrick Duffy is working toward a Computer Science and Engineering degree at the J.B. School of Engineering at the University of Louisville and is planning on completing his Masters of Computer Science and Engineering and then moving on to getting a Electrical Engineering bachelors degree.  The desire to understand the relationship between circuits and lower level programming is a fascinating field and he looks forward to leaning a great deal by working with the rocket team to design a fascinating object.
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/sam.jpg" class="team" /><br>
								<span>Samuel Miller</span>
							</div>
							<div id="member_desc">
								Sam is a senior at the J.B. Speed school of Engineering majoring in Mechanical Engineering. He became interested in the rocketry team after a informational meeting last year. He is currently contributing as a member of the rocket payload integration team which includes designing the rover fairing. He is interested in a career in the aerospace industry.
							</div>
						</div>
						<!--<div id="member">
							<div id="member_pic">
								<img src="/img/RCREMBLEM4.png" class="team" /><br>
								<span>Sarah Morris</span>
							</div>
							<div id="member_desc">
								Info coming soon...
							</div>
						</div>-->
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/sherman.jpg" class="team" /><br>
								<span>Sherman Dowell</span>
							</div>
							<div id="member_desc">
								Sherman is from Breckinridge County, Kentucky.  He is studying electrical engineering at J.B. Speed School of Engineering with junior standing.  He was interested in flight and aerospace at a young age, and wanted to gain experience in the field through the rocketry team.  He is working to design the motor control and power regulation circuitry for the rover payload that will be riding inside the rocket. 
							</div>
						</div>
						<div id="member">
							<div id="member_pic">
								<img src="/img/Team/zach.jpg" class="team" /><br>
								<span>Zack Wright</span>
							</div>
							<div id="member_desc">
								Zack is from Versailles, Kentucky and is currently in his third year at the University of Louisville J.B. Speed School of Engineering. He has always been interested in rockets and space. In high school, he was selected to the Support Team on the Kentucky Satellite Project (KySat). In working on this in high school he came looking to continue his passion for aerospace at UofL. Zack is also a brother of Triangle Fraternity, a nationwide social fraternity of Engineers, Architects, and Scientists.   
							</div>
						</div>
					</div>

				<!--<ul style="margin-left:25px;">
					<li>Austin Lassell</li>
					<li>Benjamin Mueller</li>
					<li>Justin Lord</li>
					<li>Dhwani Shah</li>
					<li>Carlos Gonzalez</li>
					<li>James Claypool</li>
					<li>Daniel Neel</li>
					<li>Emily Robison</li>
					<li>Gregg Blincoe</li>
					<li>Kareem Moulana</li>
					<li>Patrick Duffy</li>
					<li>Samuel Miller</li>
					<li>Sarah Morris</li>
					<li>Sherman Dowell</li>
					<li>Zachary Wright</li>
				</ul> -->
				</span>
			</div>			
		</div>
		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'); ?>	
<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/main-footer.php'); ?>