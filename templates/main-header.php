<?php 
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/struct.req.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>River City Rocketry</title>
		<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
		<link href="/css/main.css" rel="stylesheet" type="text/css" />
		<link href="themes/1/js-image-slider.css" rel="stylesheet" type="text/css" />
		<link href="/libraries/css/lightbox.css" rel="stylesheet" />
    		<script src="themes/1/js-image-slider.js" type="text/javascript"></script>
		<link href='http://fonts.googleapis.com/css?family=Unica+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Kelly+Slab' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Graduate' rel='stylesheet' type='text/css'>
		<script src="/libraries/js/jquery-1.10.2.min.js"></script>
		<script src="/libraries/js/lightbox-2.6.min.js"></script>
		
		<!--[if lt IE 7]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
		<![endif]-->