<div id="side">
	<div id="sidebar_no_title">
		<div id="cont">There are currently <b>
			<?php 
				 if(getActiveSessions($conn) < 0) {
					echo "0";
				 } else {
					echo getActiveSessions($conn); 
				 }
			?></b> users active.
			<br><br><center><a href="projects.php?a=teamCalendar" style="text-decoration:underline;font-weight:bold;">Team events Calander</a></center>
			<center><a href="projects.php?a=supporters" style="text-decoration:underline;font-weight:bold;line-height:24px;">Our sincere supporters!</a></center>
		</div>
	</div>
	
	<div id="sidebar_no_title" style="border:none">
		<iframe style="margin-left:-5px;border:none;" frameborder="0" height="380" scrolling="no" src="https://www.kickstarter.com/projects/352995238/university-of-louisville-nasa-student-launch-2013/widget/card.html" width="220"></iframe>
	</div>
	
	<?php
		if(loggedIn() && (getAnyUsersField_userid($conn, $_SESSION['user_id'], "admin") == 1)) {
			echo '<div id="sidebar_with_title">
					<div id="title">Admin tools</div>
					<div id="cont">
						<ul>
							<a href="blog.php"><li>Add a blog post</li></a>
						</ul>
					</div>
				  </div>';
		}
	?>
	<div id="sidebar_with_title">
		<div id="title">Project Updates</div>
		<div id="cont">
			<ul>
				<!--<li>Teleconfrence with NASA</li>
				<li>USL is back on!</li>-->
				<?php getBlogPostTitleForSidebar($conn); ?>
			</ul>
			<div class="more_link"><a href="http://rivercityrocketry.org/blog.php">Check out the blog</a></div>
		</div>
	</div>
	<div id="sidebar_with_title">
		<div id="title">Upcoming Educational Outreach Events</div>
		<div id="cont">
			<ul>
				<li>Olmstead North Middle School Outreach</li>
				<li>Ramsey Middle School Outreach</li>
			</ul>
			<div class="more_link"><a href="http://rivercityrocketry.org/outreach.php">more</a></div>
		</div>
	</div>
</div>