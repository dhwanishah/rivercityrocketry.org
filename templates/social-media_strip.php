<div id="social-media_strip">
	<ul>
		<li><a href="https://www.facebook.com/rivercityrocketry" target="_blank"><img src="/img/social-media/Facebook-icon.png" /></a></li>
		<li><a href="https://twitter.com/uoflusli" target="_blank"><img src="/img/social-media/Twitter-icon.png" /></a></li>
		<li><a href="http://www.youtube.com/uoflusli" target="_blank"><img src="/img/social-media/Youtube-icon.png" /></a></li>
		<li><a href="https://plus.google.com/118137627175902842974" rel="publisher" target="_blank"><img src="/img/social-media/GooglePlus-icon.png" /></a></li>
		<li><a href="https://www.kickstarter.com/projects/352995238/university-of-louisville-nasa-student-launch-2013" target="_blank"><img src="/img/social-media/Kickstarter-icon.png" /></a></li>
	</ul>
</div>