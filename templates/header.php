<div id="header">
	<span id="head_content">
		<div id="nav_left">
			<ul>
				<li><a href="http://rivercityrocketry.org">Home</a></li>
				<li><a href="http://rivercityrocketry.org/projects.php">Projects</a></li>
				<li><a href="http://rivercityrocketry.org/documents.php">Documents</a></li>
				<!--<div class="rocket_eff"></div>-->
			</ul>
				
		</div>
		<a href="http://rivercityrocketry.org"><div id="logo"></div></a>
		<div id="nav_right">
			<ul>
				<li><a href="http://rivercityrocketry.org/outreach.php">Outreach</a></li>
				<li><a href="http://rivercityrocketry.org/media.php">Media</a></li>
				<li><a href="http://rivercityrocketry.org/team.php">Team</a></li>
				<!--<div class="rocket_eff2"></div>-->
			</ul>
			<!--<div class="rocket"></div>-->
		</div>
		<div id="login">
			<span id="login_link">
				<a href="">
					<?php 
						if(loggedin()){
							echo getAnyUsersField_userid($conn, $user_id, 'email');
						} else {
							echo 'Login';
						}
					?>
				</a>
			</span>
			<div id="login_form">		
				<div id="content_arrow_border"></div>
				<div id="content_arrow"></div>
				
				<?php if(!loggedin()) { ?>
					<form name="lform" id="log-in_form" autocomplete="off">
						<input type="text" id="email-l" name="email-l" placeholder="Email" /><br>
						<input type="password" id="password-l" name="password-l" placeholder="Password" /><br>
						<input type="submit" id="submit_login" name="submit_login" value="Login" style="cursor:pointer;margin-left:2px;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;" />
						<a href="/register.php" style="font-size:12px;color:red;text-decoration:none;float:left;margin-top:10px;margin-left:10px;">Register</a><br><br>
						<div id="feedback_l"></div>
					</form>
					
				<?php } else {
					echo '<a href="/core/auth/logout.php">Logout</a>';
				      }
				?>
			</div>
		</div>
	</span>
</div>