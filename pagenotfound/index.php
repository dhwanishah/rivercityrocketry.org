<?php 
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/struct.req.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/core.req.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>River City Rocketry</title>
		<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
		<link href="/css/main.css" rel="stylesheet" type="text/css" />
		<link href="themes/1/js-image-slider.css" rel="stylesheet" type="text/css" />
    		<script src="themes/1/js-image-slider.js" type="text/javascript"></script>
		<link href='http://fonts.googleapis.com/css?family=Unica+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Kelly+Slab' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Graduate' rel='stylesheet' type='text/css'>
		
		<!--[if lt IE 7]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
		<![endif]-->
		<!--[if lt IE 8]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
		<![endif]-->
	</head>
<body>
<style>

</style>
<!--<div id="top-pattern"></div>-->

<div id="main_container">

	<?php include($_SERVER['DOCUMENT_ROOT'] . '/templates/social-media_strip.php'); ?>
	
	<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'); ?>
	
	<div id="content">
		<div id="main">	
			
			<div id="body">
				<h2>The page you are looking for is either gone or doesn't exist!</h2>
				<span id="body_text">
					
				</span>
			</div>			
		</div>
		
		<?php require($_SERVER['DOCUMENT_ROOT'] . '/templates/sidebar.php'); ?>
		
	</div>
	
	<div id="footer">
		University of Louisville<br>&copy; River city Rocketry
	</div>
	
</div>


<script src="https://code.jquery.com/jquery.js"></script>
<script src="/js/auth.ajax.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
	login_dropdown();
});

function login_dropdown() {
	$('#login_link').click(function() {
		$('#login_form').toggle();		
		return false;
	});
}



</script>
</body>
</html>