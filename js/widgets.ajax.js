$(document).ready(function() {
	$('#sec_check-r').hide();
	blog_submit();
});

function blog_submit() {
	$('#blog_title').focus(); //focus on username field on page load
		$('#feedback_b').load('/core/widgets/blog_post.php').fadeIn(1000);
		$('#submit_blog').click(function () {
			$.ajax({
				type: 'POST',
				url: '/core/widgets/blog_post.php',
				data: {	blog_title: $('#blog_title').val(), blog_msg: $('#blog_msg').val(), ytVideo: $('#ytVideo').val()},
				beforeSend: function() {
			            $('#feedback_b').html('<img src="../img/load.gif" alt="Loading..."/>');
			        },
				success: function(result) {
					$('#feedback_b').html(result).fadeIn(1000);
				}
			});
	 		return false;
		});
}

function textCount(textField, showCount) {
	var maxChars = 255;
	var textField = document.getElementById(textField);
	var showCount = document.getElementById(showCount);
	if (textField.value.length > maxChars) {
		textField.value = textField.value.substring(0, maxChars);
	} else {
		showCount.innerHTML = (maxChars - textField.value.length) + " characters remaining";
	}	
}