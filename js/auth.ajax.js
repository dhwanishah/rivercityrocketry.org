$(document).ready(function() {
	$('#sec_check-r').hide();
	login_submit();
	register_submit();
});

function login_submit() {
	$('#email_l').focus(); //focus on username field on page load
		$('#feedback_l').load('/core/auth/login.php').fadeIn(1000);
		$('#submit_login').click(function () {
			$.ajax({
				type: 'POST',
				url: '/core/auth/login.php',
				data: {	email: $('#email-l').val(), password: $('#password-l').val()},
				beforeSend: function() {
			            $('#feedback_l').html('<img src="../img/load.gif" alt="Loading..."/>');
			        },
				success: function(result) {
					$('#feedback_l').html(result).fadeIn(1000);
				}
			});
	 		return false;
		});
}

function register_submit() {
	$('#email').focus(); //focus on username field on page load
		$('#feedback_r').load('/core/auth/register.php').fadeIn(1000);
		$('#submit_register').click(function () {
			$.ajax({
				type: 'POST',
				url: '/core/auth/register.php',
				data: {	email: $('#email-r').val(), password: $('#password-r').val(), 
					confirm_password: $('#confirm_password-r').val(), sec_check: $('#sec_check-r').val() },
				beforeSend: function() {
			            $('#feedback_r').html('<img src="../img/load.gif" alt="Loading..."/>');
			        },
				success: function(result) {
					$('#feedback_r').html(result).fadeIn(1000);
				}
			});
	 		return false;
		});
}